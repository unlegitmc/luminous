package me.liuli.luminous.agent

import javassist.CtClass
import me.liuli.luminous.Luminous
import me.liuli.luminous.agent.hook.HookManager
import me.liuli.luminous.agent.hook.impl.Hook
import me.liuli.luminous.agent.hook.impl.HookFunction
import me.liuli.luminous.agent.hook.impl.HookReturnInfo
import me.liuli.luminous.agent.hook.impl.HookType
import me.liuli.luminous.utils.jvm.AccessUtils
import me.liuli.luminous.utils.misc.logError
import java.lang.instrument.Instrumentation
import java.net.URL
import java.net.URLClassLoader

object Agent {
    lateinit var instrumentation: Instrumentation
        private set

    var forgeEnv = false
        private set

    /**
     * called from [Luminous.agentmain]
     */
    fun main(agentArgs: String, instrumentation: Instrumentation) {
        this.instrumentation = instrumentation

        AccessUtils.initWithAutoVersionDetect()

        // forge wrapped the namespace, so we need to use reflection to get the real classloader and load the classes into real namespace
        var forgeFlag = false

        try {
            val classLoader = AccessUtils.getClassByName("net.minecraftforge.fml.common.Loader").classLoader
//            val classLoader = clazz.getDeclaredMethod("getModClassLoader")
//                .invoke(clazz.getDeclaredMethod("instance").invoke(null)) as ClassLoader

            val method = URLClassLoader::class.java.getDeclaredMethod("addURL", URL::class.java)
            method.isAccessible = true
            method.invoke(classLoader, Luminous.jarFileAt.toURI().toURL())

            forgeFlag = true
        } catch (ignore: ClassNotFoundException) { } catch (t: Throwable) { t.printStackTrace() }

        loadHooks()
        HookManager.inject(instrumentation)

        if(!forgeFlag) {
            init()
        }
    }

    /**
     * load hooks for the cheat
     */
    private fun loadHooks() {
        AccessUtils.getReflects("me.liuli.luminous.hooks", HookFunction::class.java)
            .forEach {
                try {
                    HookManager.registerHookFunction(it.newInstance())
                } catch (e: IllegalAccessException) {
                    // this module looks like a kotlin object
                    AccessUtils.getKotlinObjectInstance(it)?.let { function ->
                        HookManager.registerHookFunction(function)
                    }
                } catch (e: Throwable) {
                    logError("Failed to load hook: ${it.name} (${e.javaClass.name}: ${e.message})")
                }
            }
    }

    /**
     * called for namespace switch for forge
     */
    fun initForForge() {
        forgeEnv = true

        AccessUtils.initWithAutoVersionDetect()

        loadHooks()

        init()
    }

    /**
     * final initialization for the cheat
     */
    fun init() {
    }
}